/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andre
 */
public class Venda extends Entidade{
    private ArrayList<Produto> saida;
    private Cliente cliente;
    private float valor;
    private String datareg;
    
    public Venda(int numero, ArrayList<Produto> saida, Cliente cliente, float valor, String datareg) {
        super(numero);
        this.saida = saida;
        this.cliente = cliente;
        this.valor = valor;
        this.datareg = datareg;
    }
    
    public List<Produto> getSaida() {
        return saida;
    }

    public void setSaida(ArrayList<Produto> saida) {
        this.saida = saida;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getDatareg() {
        return datareg;
    }

    public void setDatareg(String datareg) {
        this.datareg = datareg;
    }
    
}
